<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Nicolas',
            'email'    => 'nick1321@gmail.com',
            'password' => bcrypt('123456'),
            'role'     => 0

    ]);


        User::create([
        'name'     => 'Victor',
        'email'    => 'vicgal@gmail.com',
        'password' => bcrypt('123456'),
        'role'     => 1

    ]);

        User::create([
            'name'     => 'Jorge',
            'email'    => 'jorge@gmail.com',
            'password' => bcrypt('123456'),
            'role'     => 2

        ]);

    }
}
