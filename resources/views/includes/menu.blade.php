<div class="card">
    <div class="card-header bg-primary" style="color: white">Menú</div>

    <div class="card-body">

        <ul class="nav navbar-link nav-pills nav-tabs">

            @if(auth()->check())
                <li><a href="#" class="nav-link">Dashboard</a></li>
                <li><a href="#" class="nav-link">Ver incidencias</a></li>
                <li><a href="#" class="nav-link">Reportar incidencia</a></li>

                <ul class="dropdown nav nav-pills nav-tabs">

                        <a href="#" class=" nav-link dropdown-toggle" data-toggle="dropdown">Administración</a>

                    <li class="dropdown-menu">
                        <a class="dropdown-item" href="/usuarios">Usuarios</a>
                        <a class="dropdown-item" href="/proyectos">Proyectos</a>
                        <a class="dropdown-item" href="/config">Configuración</a>
                    </li>
                </ul>


            @else
                <li class="nav-item"><a href="#" class="nav-link ">Bienvenido</a></li>
                <li class=""><a href="#" class="nav-link">Instrucciones</a></li>
                <li class=""><a href="#" class="nav-link">Créditos</a></li>
            @endif
        </ul>
    </div>
</div>




